#include "Rii.h"

// Este archivo incluye las funciones para obtener informacion de vertices de un grafo G.
// Todas las funciones son de O(1).

// Calculo la cantidad de vertices del grafo G.
u32 NumeroDeVertices(Grafo G){
  return G->vn;
};

// Calculo la cantidad de lados del grafo G.
u32 NumeroDeLados(Grafo G){
  return G->ln;
};

// Calculo la cantidad de colores del grafo G.
u32 NumeroDeColores(Grafo G){
  return G->cn;
};

// Devuelvo el nombre de un vertice en la posicion 'i' del grafo G.
u32 NombreDelVertice(Grafo G, u32 i){
  return G->vs[i]->vname;
};

// Dado un grafo G y un vertice 'i' devuelvo su color.
u32 ColorDelVertice(Grafo G, u32 i){
  if(G->vn <= i){
    return ((2^32) - 1);
  }
  return G->vs[i]->color;
};

// Dado un grafo G y un vertice 'i' devuelvo la cantidad de vecinos
// del vertice 'i'.
u32 GradoDelVertice(Grafo G, u32 i){
  if(G->vn <= i){
    return ((2^32) - 1);
  }
  return G->vs[i]->nvecinos;
};

// Dado un vertice 'i' y su vecino 'j' en un grafo G, devuelvo el color del
// vecino 'j' del vertice 'i.
u32 ColorJotaesimoVecino(Grafo G, u32 i, u32 j){
  Vertice v = G->vs[i];
  if(G->vn <= i || v->nvecinos <= j){
    return ((2^32) - 1);
  }
  return v->vecinos[j]->color;
};

// Dado un vertice 'i' y su vecino 'j' en un grafo G, devuelvo el nombre del
// vecino 'j' del vertice 'i.
u32 NombreJotaesimoVecino(Grafo G, u32 i,u32 j){
  Vertice v = G->vs[i];
  return v->vecinos[j]->vname;
};
