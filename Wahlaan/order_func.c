#include "Rii.h"

// order_func.c contiene las implementaciones de las funciones para
// reordenar los vertices de un grafo G.

// Funcion 'compare' usada por qsort para comparar dos elementos y ordenarlos
// segun su nombre.
int comparenatural(const void * a, const void * b)
{
  const struct VerticeSt **v1 = (const struct VerticeSt **)a;
  const struct VerticeSt **v2 = (const struct VerticeSt **)b;
  if((*v1)->vname < (*v2)->vname){
    return -1;
  } else if((*v1)->vname > (*v2)->vname){
    return 1;
  } else{
    return 0;
  }
};

// Funcion 'compare' usada por qsort para comparar dos elementos y ordenarlos
// segun su grado.
int comparewelshpowell(const void * a, const void * b)
{
  const struct VerticeSt **v1 = (const struct VerticeSt **)a;
  const struct VerticeSt **v2 = (const struct VerticeSt **)b;
  return (*v2)->nvecinos - (*v1)->nvecinos;
};

char OrdenWelshPowell(Grafo G){
  // Utilizo la funcion qsort para ordenar los vertices segun la cantidad de vecinos.
  qsort(G->vs, G->vn, sizeof(Vertice), comparewelshpowell);
  // Cambio el orden interno de los vertices en el grafo G.
  for(u32 i=0; i<G->vn; i++){
    G->vs[i]->pos_i=i;
  }
  return 0;
};

char OrdenNatural(Grafo G){
  // Utilizo la funcion qsort para ordenar los vertices segun el nombre del vertice.
  qsort(G->vs, G->vn, sizeof(Vertice), comparenatural);
  // Cambio el orden interno de los vertices en el grafo G.
  for(u32 i=0; i<G->vn; i++){
    G->vs[i]->pos_i=i;
  }
  return 0;
};

char SwitchVertices(Grafo G,u32 i,u32 j){
  // Chequeamos si los indices entran en el rango del Grafo.
  if(i<G->vn && j<G->vn){
    Vertice tmp;
    tmp = G->vs[i];
    G->vs[i]=G->vs[j];
    G->vs[j]=tmp;
    // Seteamos las nuevas posiciones de los vertices.
    G->vs[i]->pos_i=i;
    G->vs[j]->pos_i=j;
    return 0;
  } else {
    return 1;
  }
};

// Funcion 'compare' usada por qsort para comparar dos elementos y ordenarlos
// segun su color.
int comparermbcnormal(const void * a, const void * b){
  const struct VerticeSt **v1 = (const struct VerticeSt **)a;
  const struct VerticeSt **v2 = (const struct VerticeSt **)b;
  return (*v1)->color - (*v2)->color;
};

char RMBCnormal(Grafo G){
  // Utilizo qsort con una funcion de comparacion que utiliza los colores de los vertices.
  qsort(G->vs, G->vn, sizeof(Vertice), comparermbcnormal);
  // Cambio el orden interno de los vertices en el grafo G.
  for(u32 i=0; i<G->vn; i++){
    G->vs[i]->pos_i=i;
  }
  return 0;
};

int comparermbcrevierte(const void * a, const void * b){
  const struct VerticeSt **v1 = (const struct VerticeSt **)a;
  const struct VerticeSt **v2 = (const struct VerticeSt **)b;
  return (*v2)->color - (*v1)->color;
};

char RMBCrevierte(Grafo G){
  // Esta funcion es parecida a RMBCnormal pero invierte la comparacion.
  qsort(G->vs, G->vn, sizeof(Vertice), comparermbcrevierte);
  for(u32 i=0; i<G->vn; i++){
    G->vs[i]->pos_i=i;
  }
  return 0;
};

int comparechicogrande(const void * a, const void * b){
  const struct VerticeSt **v1 = (const struct VerticeSt **)a;
  const struct VerticeSt **v2 = (const struct VerticeSt **)b;
  // Chequeo el atributo 'samecolor' para saber quien tiene mas 'hermanos vertices'.
  if((*v1)->samecolor < (*v2)->samecolor){
    return -1;
  } else if((*v1)->samecolor > (*v2)->samecolor){
    return 1;
  } else {
    // Si ambos colores de los vertices tienen la misma cantidad de vertices asignados
    // Los ordeno por el color.
    if((*v1)->color < (*v2)->color){
      return -1;
    } else {
      return 1;
    }
  }
  return 0;
};

char RMBCchicogrande(Grafo G){
  // Creo un arreglo nuevo donde los index son el color y el elemento representa
  // la cantidad de vertices con ese color.
  u32 *nvcolor = calloc(G->cn, sizeof(u32));
  if(nvcolor){
    for(u32 i=0;i<G->vn; i++){
      nvcolor[G->vs[i]->color]++;
    }
    // Seteo el atributo 'samecolor' en los vertices del grafo G.
    for(u32 i=0; i<G->vn; i++){
      G->vs[i]->samecolor = nvcolor[G->vs[i]->color];
    }
    // Ordeno el grafo utilizando la funcion de comparacion 'comparechicogrande'.
    qsort(G->vs, G->vn, sizeof(Vertice), comparechicogrande);
    
    // Seteo la nueva posicion de los vertices despues de la ordenacion.
    for(u32 i=0; i<G->vn; i++){
      G->vs[i]->pos_i=i;
    }
    
    free(nvcolor);
    return 0;
  } else {
    return 1;
  }
};

char SwitchColores(Grafo G,u32 i,u32 j){
  // Chequeo si los colores dados entran en los parametros disponibles.
  if(i<G->cn && j<G->cn){
    // Para cada vertice del color 'i', cambio ese color por 'j';
    for(u32 k=0; k<G->vn; k++){
      if(G->vs[k]->color == i){
        G->vs[k]->color = j;
      } else if(G->vs[k]->color == j){
        G->vs[k]->color = i;
      }
    }
    return 0;
  } else {
    return 1;
  }
};