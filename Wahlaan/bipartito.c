#include "Rii.h"

// Este archivo .c contiene la funcion principal para la ejecución de bipartito

//////////////////////// AUXILIARES ////////////////////////

// Remueve los colores del grafo
void DespintarGrafo(Grafo G) {
  if (G != NULL) {
    for (u32 i = 0; i < G->vn; i++) {
        G->vs[i]->color = 0;
    }
  }
};

///////////////// Implementación de cola ///////////////////

//// Tipos ////

typedef struct ColaSt *Cola;
typedef struct ListSt *List;

typedef struct ColaSt { 
    List cola;  
    List last;  
}ColaSt;

// Lista enlazada de vertices
typedef struct ListSt {
    Vertice v;  
    List next; 
}ListSt;

//// Funciones y procedimientos ////

// Encola la referencia de un vertice al final de la cola 
// (lo ubica como primer elemento).
// Asigna la cola en memoria en caso de que aun no lo esté
Cola Encolar(Cola queue, Vertice x) {
    List new_v = calloc(1, sizeof(struct ListSt));
    new_v->v = x;
    new_v->next = NULL;

    if (queue == NULL) 
      queue = calloc(1,sizeof(struct ColaSt));

    if (queue->cola == NULL) {
      queue->cola = new_v;
      queue->last = new_v;
    } else {
      queue->last->next = new_v;
      queue->last = queue->last->next;
    }

    return queue;
};

// Desencola un vertice y lo retorna 
Vertice Desencolar(Cola queue) {
  Vertice bottom = NULL;
  if (queue != NULL && queue->cola != NULL) {
      bottom = queue->cola->v;
  }
  List aux = queue->cola;
  queue->cola = queue->cola->next;
  aux->v = NULL;
  aux->next = NULL;
  free(aux);

  return bottom;
};

// Destruye la cola y libera la memoria
void DestruirCola(Cola queue) {
    List aux = NULL;
    List next = NULL;

    if (queue != NULL) {
        queue->last = NULL;
        if (queue->cola != NULL) {
            aux = queue->cola;
            while (aux != NULL) {
                next = aux->next;
                free(aux);
                aux = next;
            }
            next = NULL;
            aux = NULL;
        }
        queue->cola = NULL;
    }
    free(queue);
    queue = NULL;
};

// Checkea si la cola esta vacia
bool ColaVacia(Cola queue) {
    bool r;
    if (queue != NULL && queue->cola != NULL) {
        r = false;
    } else {
      r = true;
    }
    return r;
};
////////////////////// END AUXILIARES //////////////////////

//////////////////////// PRINCIPAL ////////////////////////

// Funcion que checkea si un grafo es bipartito
// Devuelve 1 si G es bipartito, 0 si no.
// Si es bipartito, deja a G coloreado en forma propia con los colores 0 y 1.
// Si devuelve 0, antes colorea a G con Greedy, en orden WelshPowell   
u32 Bipartito(Grafo G) {
  DespintarGrafo(G);
  Cola cola = NULL;
  u32 VerticesColoreados = 0; 
  Vertice p = NULL;
  for (u32 i = 0; VerticesColoreados < G->vn; i++) {
    if (G->vs[i]->color == 0) {
      G->vs[i]->color = 1;
      VerticesColoreados++; 
      cola = Encolar(cola, G->vs[i]);
      while (!ColaVacia(cola)) {
        p = Desencolar(cola);
        for (u32 i = 0; i < p->nvecinos; i++) {
          if (p->vecinos[i]->color == 0) {
            p->vecinos[i]->color = 3 - p->color;
            cola = Encolar(cola, p->vecinos[i]);
            VerticesColoreados++;
          }
          if (p->color == p->vecinos[i]->color && p->color != 0) {
            // El grafo no es bipartito, coloreamos G con Greedy en orden
            // WelshPowel y retorna 0. Destruye la cola
            p = NULL;
            DestruirCola(cola);
            cola = NULL;
            OrdenWelshPowell(G);
            Greedy(G);
            return 0;
          }
        }
      }
    }
  }
  // El grafo es bipartito, corregimos los colores y retornamos 1. 
  // Destruimos la cola 
  p = NULL;
  DestruirCola(cola);
  cola = NULL;
  G->cn = 2;
  for (u32 i = 0; i < G->vn; i++) {
    if (G->vs[i]->color == 2) {
      G->vs[i]->color = 0;
    }
  }
  return 1;
};