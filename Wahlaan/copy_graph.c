#include "Rii.h"

// Este archivo .c incluye la funcion par copiar grafo.

// CopiarGrafo toma un grafo G y devuelve un grafo 'gcopy' el cual es igual que G.
Grafo CopiarGrafo(Grafo G){
  Grafo gcopy = NULL;
  gcopy = calloc(1, sizeof(struct GrafoSt));
  gcopy->vn = G->vn;
  gcopy->ln = G->ln;
  gcopy->cn = G->cn;
  gcopy->vs = calloc(G->vn, sizeof(Vertice));
  
  // Copio la estructura de los vertices.
  for(u32 i=0; i<(G->vn); i++){
    gcopy->vs[i] = malloc(sizeof(struct VerticeSt));
    (gcopy->vs[i])->vname = (G->vs[i])->vname;
    (gcopy->vs[i])->pos_i = (G->vs[i])->pos_i;
    (gcopy->vs[i])->nvecinos = (G->vs[i])->nvecinos;
    (gcopy->vs[i])->color = (G->vs[i])->color;
    (gcopy->vs[i])->vecinos = calloc((gcopy->vs[i])->nvecinos, sizeof(Vertice));
    (gcopy->vs[i])->samecolor = (G->vs[i])->samecolor;
  }

  // Copio los punteros a los vecinos.
  for(u32 i=0; i<(G->vn); i++){
    Vertice v2 = gcopy->vs[i];
    Vertice v1 = G->vs[i];
    for(u32 j=0; j<(v1->nvecinos); j++){
      u32 index = v1->pos_i;
      v2->vecinos[j] = gcopy->vs[index];
    }
  }

  // Devuelvo el puntero al grafo nuevo.
  return gcopy;
};
