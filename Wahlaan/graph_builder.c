#include "Rii.h"

// Este archivo .c contiene la funcion principal para parsear y crear el grafo.

//////////////////////// AUXILIARES ////////////////////////
// Func compare usada por qsort, esta compara dos u32,
// en este caso ordena de menor a mayor 'a' y 'b'.
int compareu32 (const void * a, const void * b){
  if(*(u32*)a > *(u32*)b){
    return 1;
  } else if(*(u32*)a < *(u32*)b){
    return -1;
  } else {
    return 0;
  }
};

// Tomando dos punteros a la estructura VerticeSt los enlaza
// como vecinos.
void add_neigh(Vertice vs1, Vertice vs2){
  u32 vecinos_size = vs1->nvecinos * sizeof(Vertice);
  vs1->vecinos = realloc(vs1->vecinos, vecinos_size + sizeof(Vertice));
  vs1->vecinos[vs1->nvecinos] = vs2;
  vs1->nvecinos++;

  vecinos_size = vs2->nvecinos * sizeof(Vertice);
  vs2->vecinos = realloc(vs2->vecinos, vecinos_size + sizeof(Vertice));
  vs2->vecinos[vs2->nvecinos] = vs1;
  vs2->nvecinos++;  
};

// Una modificacion al algoritmo de busqueda binaria para encontrar 
// un vertice llamado 'x' en un arreglo llamada 'varray'.
u32 binary_search_v(Vertice *varray, u32 l, u32 r, u32 x){
  if(r>=l){
    u32 mid = (l + r) / 2;
    if(varray[mid]->vname==x){
      return mid;
    } else if(varray[mid]->vname>x){
      return binary_search_v(varray, l, mid-1, x);
    } else {
      return binary_search_v(varray, mid+1, r, x);  
    }
  }
  return 0;
};

// Esta funcion es usada para comparar el numero de vertices de 'p edge'
// con la cantidad de vertices obtenidos del parseo de datos.
u32 cant_vertices(Vertice *varray, u32 n){
  for(u32 i=0; i<n; i++){
    if(varray[i]==0){
      return i;
    }
  }
  return n;
};

// Funcion principal para cargar dos vecinos a partir de sus nombres.
void charge_neigh(u32 p, u32 q, Vertice *varray, u32 n){
  int pos_p=binary_search_v(varray, 0, n, p);
  int pos_q=binary_search_v(varray, 0, n, q);

  add_neigh(varray[pos_p],varray[pos_q]);
};

//////////////////////// PRINCIPALES ////////////////////////
Grafo ConstruccionDelGrafo(){
  // Creamos las variables a utilizar como el grafo, las variables de parseo
  // numeros de lados, vertices etc.
	Grafo G = NULL;
	char c;
	char edge[4];
  bool flag = true;
  int eat;
  u32 n = 0, m = 0, v1 = 0, v2 = 0;

  // Con este loop nos salteamos los comentarios del archivo.
  while(flag){
    eat = scanf("%c", &c);
    if(c=='c' && eat==1){
      while(c!='\n'){
        eat = scanf("%c", &c);
      }
    } else {
      flag=false;
    }
  }

  // Si comienza el grafo obtenemos los datos de los lados y cantidad de vertices.
  // Tambien reservamos lugar para la estructura del grafo y los punteros a los vertices.
  if (scanf(" %4s", edge) && scanf(" %u",&n) && scanf(" %u",&m)){
    G = calloc(1, sizeof(struct GrafoSt));
    G->vn = n;
    G->ln = m;
    G->cn = 0;
    G->vs = calloc(n, sizeof(Vertice));
  } else {
    printf("error en primera linea sin comentario \n");
    return NULL;
  }

  // Reservamos espacio para dos arreglos nuevos, que vamos a utilizarlos para
  // Conseguir los vertices y la relacion de vecinos entre los vertices.
  u32 *input_array = calloc(2*m, sizeof(u32));
  u32 *neigh_array = calloc(2*m, sizeof(u32));
  u32 fst = 0;
  u32 scnd = 1;
  // Loop for para cargar el input en los arreglos.
  for(u32 i=0; i<m; i++){
    if (scanf(" %c", &c) && c=='e' && scanf(" %u", &v1) && scanf(" %u", &v2)){
      input_array[fst] = v1;
      neigh_array[fst] = v1;
      input_array[scnd] = v2;
      neigh_array[scnd] = v2;
      fst=fst+2;  
      scnd=scnd+2;
      c = 'x';
    } else {
      printf("error de lectura en lado %u \n", i);
      return NULL;
    }
  }

  // Ordenamos uno de los arreglos, que vamos a utilizarlo para construir los vertices
  // del grafo.
  qsort(input_array, 2*m, sizeof(u32), compareu32);

  // Este sera el arreglo de Vertice's del grafo G.
  // Reservando memoria cada vez que se necesite agregar un vertice nuevo.
  Vertice *varray = G->vs;
  u32 j=0;
  for(u32 i=0; i<(2*m)-1; i++){
    if(input_array[i]!=input_array[i+1]){
      varray[j] = malloc(sizeof(struct VerticeSt));
      varray[j]->vname = input_array[i];
      varray[j]->pos_i = j;
      varray[j]->color = 0;
      varray[j]->nvecinos = 0;
      varray[j]->vecinos = NULL;
      varray[j]->samecolor = 0;
      j++;
    }
  }
  varray[j] = malloc(sizeof(struct VerticeSt));
  varray[j]->vname = input_array[(m*2)-1];
  varray[j]->pos_i = j;
  varray[j]->color = 0;
  varray[j]->nvecinos = 0;
  varray[j]->vecinos = NULL;
  varray[j]->samecolor = 0;

  // Ahora vamos a usar el otro arreglo que no ordenamos (neigh_array) el cual nos da
  // infomacion sobre los vecinos de los vertices.
  fst = 0;
  scnd = 1;
  for(u32 j=0; j<m; j++){
    u32 v1 = neigh_array[fst];
    u32 v2 = neigh_array[scnd];
    charge_neigh(v1, v2, varray, n);
    fst=fst+2;
    scnd=scnd+2;
  }

  // Liberamos los arreglos que utilizamos ya que no los necesitaremos mas.
  free(neigh_array);
  free(input_array);

  // Corremos greedy para colorear el grafo y obtener el numero de colores.
  G->cn = Greedy(G);
  // Devolvemos el puntero al Grafo en cuestion.
  return G;
};

// Ademas de construir el grafo tambien tenemos las funciones para destruirlo y liberar el espacio
// utilizado.

// Funcion que libera el espacio utilizado por el vertice V.
void DestruccionDeVertice(Vertice V){
  free(V->vecinos);
  free(V);
};

// Funcion que libera el espacio utilizado por el grafo G.
void DestruccionDelGrafo(Grafo G){
    for(u32 i=0; i<(G->vn); i++){
      DestruccionDeVertice(G->vs[i]);
    }
    free(G->vs);
    free(G);
};
