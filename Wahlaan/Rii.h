/* Discreta II */

/* Alumnos:
	Borda Nahuel (bordanah@gmail.com).
	Avila Vazquez Simon (simonavilav@gmail.com).
*/

#ifndef _RII_H
#define _RII_H

#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <assert.h>

// Tipo u32 que usaremos es un entero sin signo.
typedef unsigned int u32;

typedef struct VerticeSt *Vertice;
typedef struct GrafoSt *Grafo;

// Estructura de cada vertice.
typedef struct VerticeSt{
	// Nombre.
	u32 vname;
	// Posicion en el array.
	u32 pos_i;
	// Vecinos.
	Vertice *vecinos;
	// Grado.
	u32 nvecinos;
	// Color.
	u32 color;
	// Cantidad de vertices con el mismo color.
	u32 samecolor;
}VerticeSt;

// Estructura del grafo.	
typedef struct GrafoSt{
	// Cantidad de vertices.
	u32 vn;
	// Cantidad de lados.
	u32 ln;
	// Cantidad de colores.
	u32 cn;
	// Arreglo de vertices (VerticeSt).
	Vertice *vs;
}GrafoSt;

// Funciones.


// Manipulacion de grafo.
Grafo ConstruccionDelGrafo();

void DestruccionDelGrafo(Grafo G);

Grafo CopiarGrafo(Grafo G);

u32 Greedy(Grafo G);

u32 Bipartito(Grafo G);


// Funciones del Grafo.
u32 NumeroDeVertices(Grafo G);

u32 NumeroDeLados(Grafo G);

u32 NumeroDeColores(Grafo G);


// Funciones de los Vertices.
u32 NombreDelVertice(Grafo G, u32 i);

u32 ColorDelVertice(Grafo G, u32 i);

u32 GradoDelVertice(Grafo G, u32 i);

u32 ColorJotaesimoVecino(Grafo G, u32 i,u32 j);

u32 NombreJotaesimoVecino(Grafo G, u32 i,u32 j);


//Funciones de ordenacion.
char OrdenNatural(Grafo G);

char OrdenWelshPowell(Grafo G);

char SwitchVertices(Grafo G,u32 i,u32 j);

char RMBCnormal(Grafo G);

char RMBCrevierte(Grafo G);

char RMBCchicogrande(Grafo G);

char SwitchColores(Grafo G,u32 i,u32 j);


#endif