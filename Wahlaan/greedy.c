#include "Rii.h"

// Este archivo incluye la implementacion del algoritmo Greedy para colorear Grafos.

// Func compare usada por qsort, esta compara dos u32,
// en este caso ordena de menor a mayor 'a' y 'b'.
int compare (const void * a, const void * b){
  if(*(u32*)a > *(u32*)b){
    return 1;
  } else if(*(u32*)a < *(u32*)b){
    return -1;
  } else {
    return 0;
  }
};

// Esta funcion consigue el color del vertice V.
u32 get_color(Vertice V){
  u32 c=0;
  u32 n=0;
  u32 *p=NULL;

  // Me fijo en los vertices vecinos que vienen antes que V y guardo sus colores.
  for(u32 i=0; i<V->nvecinos; i++){
    if((V->vecinos[i]->pos_i) < (V->pos_i)){
      p=realloc(p,(n+1)*sizeof(u32));
      p[n]=V->vecinos[i]->color;
      n++;
    }
  }

  // Ordeno los colores obtenidos.
  qsort(p, n, sizeof(u32), compare);
  
  // Asigno el menor color posible al vertice V.
  for(u32 j=0; j<n; j++){
    if(c<p[j]){
      V->color = c;
      break;
    } else if(c==p[j]){
      c++;
    }
  }

  // Libero los colores de los vecinos utilizados previamente.
  free(p);

  return c;
};

// Algoritmo greedy de coloreo del grafo G.
u32 Greedy(Grafo G){
  // Inicializo la variable 'k' en el color maximo obtenido hasta ese momento.
  u32 k=0;

  for(u32 i=0; i<(G->vn); i++){
    // Para cada vertice consigo su color utilizando la funcion get_color.
    u32 color = get_color(G->vs[i]);
    G->vs[i]->color = color;
    // Si aumento la cantidad de colores en el grafo cambio a la variable 'k'.
    if(G->vs[i]->color > k){
      k=G->vs[i]->color;
    }
  }
  // Como tomo al '0' como un color tengo que sumar uno para saber la cantidad de colores.
  G->cn=k+1;
  return (k+1);
};
